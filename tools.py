import psutil
import platform

def get_cpu_times():
    cpu_times = psutil.cpu_times()
    cpu_times_info = {
        "user": cpu_times.user,
        "system": cpu_times.system,
        "idle": cpu_times.idle,
    }
    os_type=platform.system()
    
    if"Windows"==os_type:
        cpu_times_info["interrupt"]=cpu_times.interrupt
        cpu_times_info["dpc"]=cpu_times.dpc
    elif"Linux"==os_type:
        cpu_times_info["iowait"]=cpu_times.iowait
        cpu_times_info["irq"]=cpu_times.irq
        cpu_times_info["softirq"]=cpu_times.softirq
    return {"cpu_times":cpu_times_info}


def get_cpu_count(log=False):
    cpu_count_info = {
        f"cpu_count(logical={log})": psutil.cpu_count(logical=log),
    }

    return {"cpu_count": cpu_count_info}



def get_loadavg():
    os_type=platform.system()
    if"Windows"==os_type:
        load_avg = psutil.getloadavg()
    elif"Linux"==os_type: 
        load_avg=psutil.cpu_freq()
    return {"getloadavg": load_avg}


def get_virtual_memory():
    virtual_memory = psutil.virtual_memory()
    return {
        "total": virtual_memory.total,
        "available": virtual_memory.available,
        "percent": virtual_memory.percent,
        "used": virtual_memory.used,
        "free": virtual_memory.free,
    }



def get_swap_memory():
    swap_memory = psutil.swap_memory()
    return {
        "total": swap_memory.total,
        "used": swap_memory.used,
        "free": swap_memory.free,
        "percent": swap_memory.percent,
        "sin": swap_memory.sin,
        "sout": swap_memory.sout
    }
