from fastapi import FastAPI
import tools

app = FastAPI()


@app.get("/cpu_times/")
def get_cpu_times():
    return tools.get_cpu_times()



@app.get("/cpu_count/")
def get_cpu_count():
    return tools.get_cpu_count()

@app.get("/cpu_count(true)/")
def get_cpu_count():
    return tools.get_cpu_count(log=True)


@app.get("/getloadavg/")
def get_loadavg():
    return tools.get_loadavg()


@app.get("/virtual_memory/")
def get_virtual_memory():
    return tools.get_virtual_memory()


@app.get("/swap_memory/")
def get_swap_memory():
    return tools.get_swap_memory()